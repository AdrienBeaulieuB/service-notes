import { ApiProperty } from '@nestjs/swagger';
import { Category, Note } from '../entities/note.entity';

export class NoteDto {
  @ApiProperty()
  id: number;
  @ApiProperty({ example: 'newTitle' })
  title: string;
  @ApiProperty({ example: 'newText' })
  text: string;
  @ApiProperty()
  author: string;
  @ApiProperty({ enum: Category })
  category: string;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  updatedAt: Date;

  constructor(value: Note) {
    this.id = value.id ?? 0;
    this.title = value.title ?? '';
    this.text = value.text ?? '';
    this.author = value.author;
    this.category = value.category ?? Category.OTHER;
    this.createdAt = value.createdAt ?? new Date();
    this.updatedAt = value.updatedAt ?? new Date();
  }
}
