import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from "class-validator";
import { Category } from '../entities/note.entity';

export class CreateNoteDto {
  @ApiProperty({ example: 'newTitle' })
  @IsNotEmpty()
  title: string;

  @ApiProperty({ example: 'newText' })
  @IsNotEmpty()
  text: string;

  @ApiProperty()
  @IsNotEmpty()
  author: string;

  @ApiProperty({ example: 'WORK' })
  @IsEnum(Category)
  category: Category;

  constructor(title: string, text: string, author: string, category: Category) {
    this.title = title;
    this.text = text;
    this.author = author;
    this.category = category;
  }
}
