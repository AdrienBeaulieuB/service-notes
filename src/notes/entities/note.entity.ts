import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

type NoteProperties = Required<Note>
export enum Category {
  WORK = 'WORK',
  PERSO = 'PERSO',
  OTHER = 'OTHER',
}

@Entity()
export class Note {
  @PrimaryGeneratedColumn()
  public id?: number;
  @Column()
  public title?: string;
  @Column()
  public text?: string;
  @Column()
  public author: string;
  @Column({
    type: 'enum',
    enum: Category,
    default: Category.OTHER,
  })
  public category: Category = Category.OTHER;
  @Column()
  createdAt: Date = new Date();
  @Column()
  updatedAt: Date = new Date();

  public static  fromProperties(value: NoteProperties): Note {
    const note = new Note();
    note.id = value.id;
    note.title = value.title;
    note.text = value.text;
    note.category = value.category;
    note.createdAt = value.createdAt;
    note.updatedAt = value.updatedAt;
    return note;
  }
}
