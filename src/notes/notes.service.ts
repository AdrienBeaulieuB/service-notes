import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Note } from './entities/note.entity';
import { Repository } from 'typeorm';

@Injectable()
export class NotesService {
  constructor(
    @InjectRepository(Note)
    private notesRepository: Repository<Note>,
  ) {}

  async create(createNoteDto: CreateNoteDto): Promise<Note> {
    return this.notesRepository.save({
      ...createNoteDto,
      createdAt: new Date(),
      updatedAt: new Date(),
    });
  }

  findAll(): Promise<Note[]> {
    return this.notesRepository.find();
  }

  async findOne(id: number): Promise<Note> {
    const note = await this.notesRepository.findOneById(id);
    if (note == null) {
      throw new NotFoundException('No note for this id');
    }
    return note;
  }

  async update(id: number, updateNoteDto: UpdateNoteDto): Promise<void> {
    const result = await this.notesRepository.update(id, {
      ...updateNoteDto,
      updatedAt: new Date(),
    });
    if (result.affected == 0) {
      throw new NotFoundException('No note for this id');
    }
  }

  async remove(id: number): Promise<void> {
    const result = await this.notesRepository.delete(id);
    if (result.affected == 0) {
      throw new NotFoundException('No note for this id');
    }
  }
}
